function initialize() {
 "use strict";
  var mapOptions = {
	zoom: 14,
	scrollwheel: false,
	center: new google.maps.LatLng(-15.8162414, -47.8989094)
  };

  var map = new google.maps.Map(document.getElementById('googleMap'),
	  mapOptions);


  var marker = new google.maps.Marker({
	position: map.getCenter(),
	animation:google.maps.Animation.BOUNCE,
	icon: 'img/map-marker.png',
	map: map
  });

}

google.maps.event.addDomListener(window, 'load', initialize);

